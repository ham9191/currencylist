$(function () {

    $.ajaxSetup({
        beforeSend: function (xhr, type) {
            if (!type.crossDomain) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
            }
        },
    });

    let syncButton = $('#sync'),
        editButton = 'table tbody td a',
        popUpCloseButton = '#pop_up .close',
        popUpEditButton = '#pop_up button';

    date();
    chart();

    function getCurrencyData() {
        $.ajax({
            url: "https://api.exchangeratesapi.io/latest?base=USD&symbols=ILS,JPY,BGN,RUB",
            success: function (result) {

                if (!result) {
                    return false;
                }

                appendToTable(result);
                storeInDb(result);
            }
        });
    }

    function appendToTable(data) {

        let tableBody = $('table tbody');
        let i = 1;

        tableBody.empty();

        $.each(data.rates, function (currency, value) {
            tableBody.append("<tr id='" + currency + "'>" +
                "<td>" + i++ + "</td>" +
                "<td>" + currency + "</td>" +
                "<td id='value'>" + value + "</td>" +
                "<td id='isEdited'>" + "No" + "</td>" +
                "<td><a data-currency='" + currency + "' " +
                "data-value='" + value + "'>Edit</a></td>" +
                "</tr>");
        });
    }

    function storeInDb(data) {

        $.ajax({
            method: "POST",
            url: "./create",
            data: JSON.stringify(data.rates),
            contentType: "json",
            processData: false,
            success: function (result) {
                return true;
            }
        });
    }


    syncButton.on('click', function (e) {
        e.preventDefault();
        getCurrencyData();
    });

    $(document).on('click', editButton, function () {

        let popUp = $('#pop_up'),
            currency = $(this).data('currency'),
            value = $(this).data('value');
        let ddd = false;

        popUp.show();

        popUp.find('p').text(currency);
        popUp.find('input').val(value);


    });

    $(document).on('click', popUpCloseButton, function () {

        let popUp = $('#pop_up');
        popUp.hide();
    });

    $(document).on('click', popUpEditButton, function (e) {

        e.preventDefault();

        let popUp = $('#pop_up'),
            currency = popUp.find('p').text(),
            value = popUp.find('input').val(),
            tableBody = $("table tbody");

        $.ajax({
            method: "POST",
            url: "./update",
            data: JSON.stringify({currency, value}),
            contentType: "json",
            processData: false,
            success: function (result) {

                let tableRow = tableBody.find("#" + currency);
                tableRow.find('#isEdited').text('Yes');
                tableRow.find('#value').text(value);
                popUp.hide();
            }
        });
    });

    function date() {
        let date = new Date(),
            pageDate = $('#dateSp'),
            month = date.getMonth() + 1,
            day = date.getDate();
        let output = date.getFullYear() + '/' +
            (month < 10 ? '0' : '') + month + '/' +
            (day < 10 ? '0' : '') + day;
        pageDate.text(output);
    }

    function chart(){
        let ctx = document.getElementById('myChart').getContext('2d'),
            tableBody = $('table tbody'),
            currencies = tableBody.data('currencies'),
            currencyLabels = [],
            currencyData = [];

        $.each(currencies, function (key, value) {
            currencyLabels.push(value.currency);
            currencyData.push(value.value);
        });

        let chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: currencyLabels,
                datasets: [{
                    label: 'USD Related',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: currencyData
                }]
            },
            options: {}
        });
    }
});