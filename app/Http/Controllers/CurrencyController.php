<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;

class CurrencyController extends Controller
{
    public function index()
    {
        $currencies = Currency::all()->toArray();

        if (!count($currencies)) {
            $currenciesData = $this->getCurrencyData();

            foreach ($currenciesData as $currency => $value) {

                $currencyTable = new Currency();
                $currencyTable->currency = $currency;
                $currencyTable->value = $value;

                $currencyTable->save();

                $currencies[$currency]['currency'] = $currency;
                $currencies[$currency]['value'] = $value;
                $currencies[$currency]['isEdited'] = 0;
            }
        }

        return view('currency', compact('currencies'));
    }

    public function create(Request $request)
    {
        if (!$request->ajax()) {
            return;
        }

        $currencies = json_decode($request->getContent(), true);

        foreach ($currencies as $currency => $value) {

            $data = Currency::select('id')->where('currency', $currency)->get();

            if (!count($data)) {

                $currencyTable = new Currency();
                $currencyTable->currency = $currency;
                $currencyTable->value = $value;

                $currencyTable->save();

                continue;
            }

            $this->updateData($currency, $value);
        }
    }

    /**
     * @param Request $request
     * @return void
     */
    public function update(Request $request)
    {
        if (!$request->ajax()) {
            return;
        }

        $currency= json_decode($request->getContent(), true);
        $this->updateData($currency['currency'], $currency['value'], true);
        return;
    }

    /**
     * @return array
     */
    private function getCurrencyData()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://api.exchangeratesapi.io/latest?base=USD&symbols=ILS,JPY,BGN,RUB");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);




        $currencies = json_decode($result, true);

        return $currencies['rates'];
    }

    /**
     * @param $currency
     * @param $value
     * @param bool $isEdited
     */
    private function updateData($currency, $value, $isEdited = false)
    {
        Currency::where('currency', $currency)
            ->update([
                'value' => $value,
                'isEdited' => $isEdited
            ]);
    }
}